#![cfg_attr(feature="std"), no_std]

mod tests;

use sp_std::{prelude::*, marker::PhantomData, ops::{Deref, DerefMut}};
use sp_io::hashing::blake2_256;
use frame_support::{
	Parameter, decl_module, decl_storage, decl_event, decl_error, ensure,
	traits::{Get, Currency, ReservableCurrency, BalanceStatus},
	weights:: Weight,
	dispatch::DispatchResult
};

use frame_system::{self as system, ensure_signed};
use codec::{Encode, Decode};
use sp_runtime::RuntimeDebug;

#[derive(Clone, Eq, PartialEq, RuntimeDebug, Encode, Decode)]
pub struct PendingSwap<T: Config>{
	pub source: T::AccountId,
	pub action: T::SwapAction,
	pub end_block: T::BlockNumber
}

pub type HashedProof = [u8; 32];

pub trait SwapAction<AccountId, T: Config>{
	fn reserve(&self, source: &AccountId) -> DispatchResult;
	
}